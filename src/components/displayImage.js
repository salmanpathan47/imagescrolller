import React from 'react';

const ImageDisplay = props => {
    const images = props.images.map(image => {
      return <img key={image.id} src={image.url} alt="Smiley face" height="42" width="42" />;
    });
  
    return <div className="image-list">{images}</div>;
  };
  
export default ImageDisplay