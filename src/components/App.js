import React from 'react';
import {Container,Row,Col} from 'react-bootstrap'
import ImageDisplay from './displayImage'
import images from './api/images'
import InfiniteScroll from 'react-infinite-scroll-component';

const style = {
  height: 500,
  border: "1px solid green",
  margin: 6,
  padding: 8
};

class App extends React.Component {
  state = {
    items: Array.from({ length: 1 })
  };

  fetchMoreData = () => {
    // a fake async api call like which sends
    // 20 more records in 1.5 secs
    setTimeout(() => {
      this.setState({
        items: this.state.items.concat(Array.from({ length: 1 }))
      });
    }, 1500);
  };

  render() {
    return (
      <div>
        <h1>Scroll</h1>
        <hr />
        <div id="scrollableDiv" style={{ height: 400, overflow: "auto" }}>
          <InfiniteScroll
            dataLength={this.state.items.length}
            next={this.fetchMoreData}
            hasMore={true}
            loader={<h4>Loading...</h4>}
            scrollableTarget="scrollableDiv"
          >
            {this.state.items.map((i, index) => (
              <div style={style} key={index}>
                div - #{index}
              </div>
            ))}
          </InfiniteScroll>
        </div>
      </div>
    );
  }
}
export default App